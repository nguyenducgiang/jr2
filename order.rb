class Order
  def initialize items, total, tax
    @items = items
    @total = 29.83
    @tax = 1.50
  end

  def items
    @items
  end

  def total
    @total
  end

  def tax
    @tax
  end
end
