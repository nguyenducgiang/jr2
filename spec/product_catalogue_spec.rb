require_relative "../product_catalogue"

describe ProductCatalogue do
  let(:catalogue) {ProductCatalogue.new}

  describe ".products" do
    subject {catalogue.products}
    it {is_expected.to be_an Array}

    describe "number of products" do
      subject {catalogue.products.count}
      it {is_expected.to eq 8}
    end

    describe "each element is an instance of Product" do
      subject {catalogue.products.all?{|p| p.is_a? Product}}
      it {is_expected.to be true}
    end
  end

  describe ".find_by_name" do
    describe "exist product" do
      subject {catalogue.find_by_name "book"}
      it  {is_expected.to be_a Product}
    end

    describe "non-exist product" do
      subject {catalogue.find_by_name "non-exist product"}
      it  {is_expected.to be nil}
    end
  end
end
