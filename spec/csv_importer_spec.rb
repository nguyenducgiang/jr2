require_relative "../csv_importer"

describe "CSVImpoter" do
  share_examples_for "csv header" do
    subject {csv.headers}
    it {is_expected.to eq ["Quantity", "Product", "Price"]}
  end

  context "input 1" do
    subject(:csv) {CSVImporter.import "csv/input_1.csv"}
    it_behaves_like "csv header"

    describe "its count" do
      subject {csv.count}
      it {is_expected.to be 3}
    end
  end

  context "input 2" do
    subject(:csv) {CSVImporter.import "csv/input_2.csv"}
    it_behaves_like "csv header"

    describe "its count" do
      subject {csv.count}
      it {is_expected.to be 2}
    end
  end

  context "input 3" do
    subject(:csv) {CSVImporter.import "csv/input_3.csv"}
    it_behaves_like "csv header"

    describe "its count" do
      subject {csv.count}
      it {is_expected.to be 4}
    end
  end
end
