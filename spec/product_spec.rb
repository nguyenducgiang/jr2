require_relative "../product"

describe Product do

  let(:product) {Product.new "book", "book", false}

  describe ".name" do
    subject {product.name}
    it {is_expected.to eq "book"}
  end

  describe ".type" do
    subject {product.type}
    it {is_expected.to eq "book"}
  end

  describe ".imported?" do
    subject {product.imported?}
    it {is_expected.to be false}
  end
end
