require_relative "../order"

describe Order do
  context "order 1" do
    let(:order) {Order.new}

    describe ".total" do
      subject {order.total}
      it {is_expected.to eq 29.83}
    end

    describe ".tax" do
      subject {order.tax}
      it {is_expected.to eq 1.50}
    end

    describe ".items" do
      
    end
  end

  context "order 2" do
    let(:order) {Order.new}

    describe ".total" do
      subject {order.total}
      it {is_expected.to eq 65.15}
    end

    describe ".tax" do
      subject {order.tax}
      it {is_expected.to eq 7.65}
    end
  end

  context "order 3" do
    let(:order) {Order.new}

    describe ".total" do
      subject {order.total}
      it {is_expected.to eq 74.68}
    end

    describe ".tax" do
      subject {order.tax}
      it {is_expected.to eq 6.70}
    end
  end
end
