require "yaml"
require_relative "product"

class ProductCatalogue
  def initialize
    data = YAML.load_file "data/products.yml"
    @products = data.map{|p| Product.new p.first, p.last["type"], p.last["imported"]}
  end

  def products
    @products
  end

  def find_by_name name
    @products.select{|p| p.name == name}.first
  end
end
