class Product
  def initialize name, type, imported
    @name = name
    @type = type
    @imported = imported
  end

  def name
    @name
  end

  def type
    @type
  end

  def imported?
    @imported
  end
end
