require "csv"
require "byebug"

class CSVImporter
  class << self
    def import csv_path
      csv = CSV.read csv_path, headers: true
    end
  end
end
